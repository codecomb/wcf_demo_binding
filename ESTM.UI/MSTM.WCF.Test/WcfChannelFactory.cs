﻿/************************************************************************************************************************
* 命名空间: MSTM.WCF.Test
* 项目描述: 
* 版本名称: v1.0.0.0
* 作　　者: 唐晓军（QQ:417281862）
* 所在区域: 北京
* 机器名称: DESKTOP-F6QRRBM
* 注册组织: 学科网（www.zxxk.com）
* 项目名称: 学易作业系统
* CLR版本:  4.0.30319.42000
* 创建时间: 2017/7/21 14:54:34
* 更新时间: 2017/7/21 14:54:34
* 
* 功 能： N/A
* 类 名： WcfChannelFactory
*
* Ver 变更日期 负责人 变更内容
* ───────────────────────────────────────────────────────────
* V0.01 2017/7/21 14:54:34 唐晓军 初版
*
* Copyright (c) 2017 Lir Corporation. All rights reserved.
*┌──────────────────────────────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．                                                  │
*│　版权所有：北京凤凰学易科技有限公司　　　　　　　　　　　　　                                                      │
*└──────────────────────────────────────────────────────────┘
************************************************************************************************************************/
using ESTM.WCF.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using ESTM.Common.DtoModel;
using System.Linq.Expressions;
using System.Xml;

namespace MSTM.WCF.Test
{
    public class WcfChannelFactory
    {
        /// <summary>  
        /// 执行方法   WSHttpBinding  
        /// </summary>  
        /// <typeparam name="T">服务接口</typeparam>  
        /// <param name="uri">wcf地址</param>  
        /// <param name="methodName">方法名</param>  
        /// <param name="args">参数列表</param>  
        public static object ExecuteMetod<T>(string uri, string methodName, params object[] args)
        {
            BasicHttpBinding binding = new BasicHttpBinding();   //出现异常:远程服务器返回错误: (415) Cannot process the message because the content type 'text/xml; charset=utf-8' was not the expected type 'application/soap+xml; charset=utf-8'.。  
            //WSHttpBinding binding = new WSHttpBinding();
            EndpointAddress endpoint = new EndpointAddress(uri);

            using (ChannelFactory<T> channelFactory = new ChannelFactory<T>(binding, endpoint))
            {
                T instance = channelFactory.CreateChannel();
                using (instance as IDisposable)
                {
                    try
                    {
                        Type type = typeof(T);
                        MethodInfo mi = type.GetMethod(methodName);
                        return mi.Invoke(instance, args);
                    }
                    catch (TimeoutException)
                    {
                        (instance as ICommunicationObject).Abort();
                        throw;
                    }
                    catch (CommunicationException)
                    {
                        (instance as ICommunicationObject).Abort();
                        throw;
                    }
                    catch (Exception vErr)
                    {
                        (instance as ICommunicationObject).Abort();
                        throw;
                    }
                }
            }
        }


        //nettcpbinding 绑定方式  
        public static object ExecuteMethod<T>(string pUrl, string pMethodName, params object[] pParams)
        {
            EndpointAddress address = new EndpointAddress(pUrl);
            Binding bindinginstance = null;
            NetTcpBinding ws = new NetTcpBinding();
            ws.MaxReceivedMessageSize = 20971520;
            ws.Security.Mode = SecurityMode.None;
            bindinginstance = ws;
            using (ChannelFactory<T> channel = new ChannelFactory<T>(bindinginstance, address))
            {
                T instance = channel.CreateChannel();
                using (instance as IDisposable)
                {
                    try
                    {
                        Type type = typeof(T);
                        MethodInfo mi = type.GetMethod(pMethodName);
                        return mi.Invoke(instance, pParams);
                    }
                    catch (TimeoutException)
                    {
                        (instance as ICommunicationObject).Abort();
                        throw;
                    }
                    catch (CommunicationException)
                    {
                        (instance as ICommunicationObject).Abort();
                        throw;
                    }
                    catch (Exception vErr)
                    {
                        (instance as ICommunicationObject).Abort();
                        throw;
                    }
                }
            }
        }


        public static void UseService<TChannel>(Action<TChannel> action)
        {
            var chanFactory = new ChannelFactory<TChannel>("*");
            TChannel channel = chanFactory.CreateChannel();
            ((IClientChannel)channel).Open();
            action(channel);
            try
            {
                ((IClientChannel)channel).Close();
            }
            catch
            {
                ((IClientChannel)channel).Abort();
            }
        }
    }


    /// <summary>  
    /// 用于调用服务的类  
    /// </summary>  
    public class MyClient : ClientBase<IUserWCFService>, IUserWCFService
    {
        public MyClient(System.ServiceModel.Channels.Binding binding, EndpointAddress edpAddr)
            : base(binding, edpAddr) { }

        public IList<DtoUser> GetAllUser()
        {
            return base.Channel.GetAllUser();
        }
    }





    /// <summary>
    /// 单例模式
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Singleton<T> where T : class, new()
    {
        static object lockt = new object();
        static T t = null;
        /// <summary>
        /// 加锁单例
        /// </summary>
        public static T Current
        {
            get
            {
                if (t == null)
                {
                    lock (lockt)
                    {
                        t = t ?? new T();
                    }
                }
                return t;
            }
        }

        /// <summary>
        /// 不加锁单例
        /// </summary>
        public static T CurrentUnLock
        {
            get
            {
                if (t == null)
                {
                    t = t ?? new T();
                }
                return t;
            }
        }
    }


}
