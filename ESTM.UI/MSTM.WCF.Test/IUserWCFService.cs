﻿using ESTM.Common.DtoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ESTM.WCF.Service.IService
{
    [ServiceContract]
    [ServiceInterface]
    public interface IUserWCFService
    {
        [OperationContract]
        IList<DtoUser> GetAllUser();
    }
}
