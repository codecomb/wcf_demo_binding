﻿/************************************************************************************************************************
* 命名空间: MSTM.WCF.Test
* 项目描述: 
* 版本名称: v1.0.0.0
* 作　　者: 唐晓军（QQ:417281862）
* 所在区域: 北京
* 机器名称: DESKTOP-F6QRRBM
* 注册组织: 学科网（www.zxxk.com）
* 项目名称: 学易作业系统
* CLR版本:  4.0.30319.42000
* 创建时间: 2017/7/21 17:02:52
* 更新时间: 2017/7/21 17:02:52
* 
* 功 能： N/A
* 类 名： DataFactory
*
* Ver 变更日期 负责人 变更内容
* ───────────────────────────────────────────────────────────
* V0.01 2017/7/21 17:02:52 唐晓军 初版
*
* Copyright (c) 2017 Lir Corporation. All rights reserved.
*┌──────────────────────────────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．                                                  │
*│　版权所有：北京凤凰学易科技有限公司　　　　　　　　　　　　　                                                      │
*└──────────────────────────────────────────────────────────┘
************************************************************************************************************************/
using MSTM.WCF.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace MSTM.WCF.Test
{
    /// <summary>
    /// 接口调用工厂
    /// </summary>
    public class DataFactory
    {
        static EndpointAddress edp = new EndpointAddress("http://127.0.0.1:8087/");
        static Binding binding = new WSHttpBinding(SecurityMode.None);
        //static object _lock = new object();
        //static DataFactory t = null;
        private DataFactory() { }

        //public static DataFactory Instance
        //{
        //    get
        //    {
        //        if (t == null)
        //        {
        //            lock (_lock)
        //            {
        //                t = t ?? new DataFactory();
        //            }
        //        }
        //        return t;
        //    }
        //}

        //static DataFactory()
        //{
        //    edp = new EndpointAddress("net.tcp://127.0.0.1:8088/");
        //    binding = new NetTcpBinding(SecurityMode.None);
        //}

        /// <summary>
        /// 获取用户实例
        /// </summary>
        /// <returns></returns>
        public static IUserWCFService GetUserService()
        {
            //ChannelFactory<IUserWCFService> factory = new ChannelFactory<IUserWCFService>(binding);         
            //return factory.CreateChannel(edp);



            ChannelFactory<IUserWCFService> factory = new ChannelFactory<IUserWCFService>(binding);
            try
            {
                var channel = factory.CreateChannel(edp);

                var clientChannel = (IClientChannel)channel;

                return channel;
            }
            catch (Exception)
            {
                factory.Abort();
                return null;
            }
            finally
            {
                
            }

        }

        private static void CloseClientChannel(IClientChannel clientChannel)
        {
            try
            {
                if (clientChannel.State != CommunicationState.Faulted)
                {
                    clientChannel.Close();
                }
            }
            catch
            {
                clientChannel.Abort();
            }
        }
    }
}
