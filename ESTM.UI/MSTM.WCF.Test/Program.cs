﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESTM.Common.DtoModel;
using System.ServiceModel;
using ESTM.WCF.Service.IService;

namespace MSTM.WCF.Test
{
    //[Export]
    class Program
    {
        //[Import]
        //public IUserRepository user { get; set; }

        static EndpointAddress edpHttp = new EndpointAddress("http://127.0.0.1:8087/");
        static EndpointAddress edpTcp = new EndpointAddress("net.tcp://127.0.0.1:8088/");

        static IUserWCFService gets()
        {
            return null;
        }


        static void Main(string[] args)
        {






















            var bbbb = ClientProxy2<IUserWCFService>.UseService<IList<DtoUser>>(a => a.GetAllUser());



            #region 使用泛型调用WCF服务 通过测试
            ClientProxy<IUserWCFService> atwzt = new ClientProxy<IUserWCFService>();

            var aaaaaa = atwzt.Use<IList<DtoUser>>("", (a) => a.GetAllUser());

            #endregion



            while (true)
            {
                var cccc = DataFactory.GetUserService().GetAllUser();
                Console.WriteLine(DateTime.Now.ToString());
            }

            Console.ReadKey();
            //var cccc = Singleton<DataFactory>.Current.GetUserService().GetAllUser();
            //var aadd = DataFactory.Instance.GetUserService().GetAllUser();

            MyClient client = new MyClient(new NetTcpBinding(SecurityMode.None), edpTcp);

            var aaaa = client.GetAllUser();


            E();

            NetTcpBindingClient();
            //UserWCFServiceClient ClientHost = new UserWCFServiceClient();

            //var a = ClientHost.GetAllUser();
            //Console.ReadLine();

            //var p = new Program();

            //try
            //{
            //    Regisgter.regisgter().ComposeParts(p);
            //}
            //catch (CompositionException cex)
            //{
            //    Console.ForegroundColor = ConsoleColor.Red;
            //    Console.WriteLine(cex.Message);
            //}



            //var a = p.user.Insert(new F_User() { name = "txj" });


        }

        public static void D()
        {
            // 创建Binding  
            WSHttpBinding httpBinding = new WSHttpBinding(SecurityMode.None);
            // 创建通道  
            ChannelFactory<IUserWCFService> factory = new ChannelFactory<IUserWCFService>(httpBinding);
            IUserWCFService channel = factory.CreateChannel(edpHttp);
            // 调用  
            //int resAdd = channel.Add(int.Parse(txtNum11.Text), int.Parse(txtNum12.Text));
            //txtResAdd.Text = resAdd.ToString();

            //int resMult = channel.Multiply(int.Parse(txtNum21.Text), int.Parse(txtNum22.Text));
            //txtResMulti.Text = resMult.ToString();

            var rand = channel.GetAllUser();
            //txtRand.Text = rand.ToString();

            // 关闭通道  
            ((IClientChannel)channel).Close();
        }


        public static void A()
        {
            // 创建Binding  
            NetTcpBinding tcpBinding = new NetTcpBinding(SecurityMode.None);
            // 创建通道  
            ChannelFactory<IUserWCFService> factory = new ChannelFactory<IUserWCFService>(tcpBinding);
            IUserWCFService channel = factory.CreateChannel(edpTcp);
            // 调用  
            //txtResAdd.Text = channel.Add(int.Parse(txtNum11.Text), int.Parse(txtNum12.Text)).ToString();
            //txtResMulti.Text = channel.Multiply(int.Parse(txtNum21.Text), int.Parse(txtNum22.Text)).ToString();
            //txtRand.Text = channel.GetRandmon().ToString();

            var aaa = channel.GetAllUser();

            // 关闭通道  
            ((IClientChannel)channel).Close();
        }


        public static void B()
        {
            WSHttpBinding httpBinding = new WSHttpBinding();
            //NetHttpBinding httpBinding = new NetHttpBinding();
            // 创建Binding  
            //WSHttpBinding httpBinding = new WSHttpBinding(SecurityMode.None);
            // 创建通道  
            ChannelFactory<IUserWCFService> factory = new ChannelFactory<IUserWCFService>(httpBinding);
            IUserWCFService channel = factory.CreateChannel(edpHttp);
            // 调用  
            //int resAdd = channel.Add(int.Parse(txtNum11.Text), int.Parse(txtNum12.Text));
            //txtResAdd.Text = resAdd.ToString();

            //int resMult = channel.Multiply(int.Parse(txtNum21.Text), int.Parse(txtNum22.Text));
            //txtResMulti.Text = resMult.ToString();

            //int rand = channel.GetRandmon();
            //txtRand.Text = rand.ToString();

            var aaa = channel.GetAllUser();

            // 关闭通道  
            ((IClientChannel)channel).Close();
        }

        public static void NetTcpBindingClient()
        {
            //NetTcpBinding绑定方式的通讯 双工  
            IUserWCFService m_Innerclient;
            ChannelFactory<IUserWCFService> m_ChannelFactory;
            NetTcpBinding binding = new NetTcpBinding();
            Uri baseAddress = new Uri(string.Format("net.tcp://{0}:{1}/IUserWCFService", "127.0.0.1", "8088"));
            m_ChannelFactory = new ChannelFactory<IUserWCFService>(binding, new EndpointAddress(baseAddress));
            m_Innerclient = m_ChannelFactory.CreateChannel();
            var htime1 = m_Innerclient.GetAllUser();

        }

        public static void E()
        {
            // 创建Binding  
            NetTcpBinding tcpBinding = new NetTcpBinding(SecurityMode.None);
            // 创建通道  
            ChannelFactory<IUserWCFService> factory = new ChannelFactory<IUserWCFService>(tcpBinding);
            IUserWCFService channel = factory.CreateChannel(edpTcp);
            // 调用  
            var aaaaaa = channel.GetAllUser();

            // 关闭通道  
            ((IClientChannel)channel).Close();
        }
    }
}
