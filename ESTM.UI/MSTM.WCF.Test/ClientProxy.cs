﻿/************************************************************************************************************************
* 命名空间: MSTM.WCF.Test
* 项目描述: 
* 版本名称: v1.0.0.0
* 作　　者: 唐晓军（QQ:417281862）
* 所在区域: 北京
* 机器名称: DESKTOP-F6QRRBM
* 注册组织: 学科网（www.zxxk.com）
* 项目名称: 学易作业系统
* CLR版本:  4.0.30319.42000
* 创建时间: 2017/7/24 8:40:40
* 更新时间: 2017/7/24 8:40:40
* 
* 功 能： N/A
* 类 名： ClientProxy
*
* Ver 变更日期 负责人 变更内容
* ───────────────────────────────────────────────────────────
* V0.01 2017/7/24 8:40:40 唐晓军 初版
*
* Copyright (c) 2017 Lir Corporation. All rights reserved.
*┌──────────────────────────────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．                                                  │
*│　版权所有：北京凤凰学易科技有限公司　　　　　　　　　　　　　                                                      │
*└──────────────────────────────────────────────────────────┘
************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Xml;

namespace MSTM.WCF.Test
{
    /// <summary>
    /// 使用泛型调用WCF服务 通过测试
    /// </summary>
    /// <typeparam name="TService">服务接口</typeparam>
    public class ClientProxy<TService>
    {

        public ClientProxy()
        {

        }

        public TReturn Use<TReturn>(string remoteAddress, Expression<Func<TService, TReturn>> operation)
        {
            var channelFactory = ChannelFactoryBuilder<TService>.BuildFromRemoteAddress(remoteAddress);
            return Use<TReturn>(channelFactory, operation);
        }

        private TReturn Use<TReturn>(ChannelFactory<TService> channelFactory, Expression<Func<TService, TReturn>> operation)
        {
            var channel = channelFactory.CreateChannel();
            var clientChannel = (IClientChannel)channel;
            clientChannel.Open();
            TReturn result = operation.Compile().Invoke(channel);
            CloseClientChannel(clientChannel);
            return result;
        }

        private void CloseClientChannel(IClientChannel clientChannel)
        {
            try
            {
                if (clientChannel.State != CommunicationState.Faulted)
                {
                    clientChannel.Close();
                }
            }
            catch
            {
                clientChannel.Abort();
            }
        }

    }

    /// <summary>
    /// 获取通道工厂
    /// </summary>
    /// <typeparam name="T"></typeparam>
    static class ChannelFactoryBuilder<T>
    {
        private static Dictionary<string, ChannelFactory<T>> channelFactories = new Dictionary<string, ChannelFactory<T>>();

        public static ChannelFactory<T> BuildFromAppConfig()
        {
            var key = typeof(T).Name;
            ChannelFactory<T> channelFactory = null;

            if (channelFactories.ContainsKey(key))
            {
                if (channelFactories.TryGetValue(key, out channelFactory))
                {
                    return channelFactory;
                }
            }

            channelFactory = new ChannelFactory<T>("*");
            channelFactories.Add(key, channelFactory);
            return channelFactory;
        }

        /// <summary>
        /// 获取远程通道
        /// </summary>
        /// <param name="remoteAddress">远程地址</param>
        /// <returns></returns>
        public static ChannelFactory<T> BuildFromRemoteAddress(string remoteAddress)
        {
            if (string.IsNullOrEmpty(remoteAddress))
            {
                remoteAddress = "http://127.0.0.1:8087/";
            }


            var key = typeof(T).Name + remoteAddress;
            ChannelFactory<T> channelFactory = null;

            if (channelFactories.ContainsKey(key))
            {
                if (channelFactories.TryGetValue(key, out channelFactory))
                {
                    return channelFactory;
                }
            }

            //NetTcp
            //var binding = new NetTcpBinding(SecurityMode.None);
            //var endpointAddress = new EndpointAddress("net.tcp://127.0.0.1:8088/");


            //WsHttp
            var binding = new WSHttpBinding(SecurityMode.None);
            var endpointAddress = new EndpointAddress("http://127.0.0.1:8087/");


            //原始
            //var binding = BuildBinding();
            //var endpointAddress = new EndpointAddress(remoteAddress);


            channelFactory = new ChannelFactory<T>(binding, endpointAddress);
            channelFactories.Add(key, channelFactory);
            return channelFactory;
        }

        /// <summary>
        /// 通信协议设置
        /// </summary>
        /// <returns></returns>
        private static Binding BuildBinding()
        {
            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            binding.MaxBufferPoolSize = int.MaxValue;
            binding.ReaderQuotas = new XmlDictionaryReaderQuotas();
            binding.ReaderQuotas.MaxDepth = int.MaxValue;
            binding.ReaderQuotas.MaxStringContentLength = int.MaxValue;
            binding.ReaderQuotas.MaxArrayLength = int.MaxValue;
            binding.ReaderQuotas.MaxBytesPerRead = int.MaxValue;
            binding.ReaderQuotas.MaxNameTableCharCount = int.MaxValue;
            return binding;
        }
    }
}
