﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSTM.WCF.Autofac
{
    public interface IDependency
    {
    }

 

    public interface IUser : IDependency
    {
        string GetUser(string model);
    }


    public class User : IUser
    {

        public User()
        {

        }

        public string GetUser(string model)
        {
            return model + "abc";
        }
    }
}
