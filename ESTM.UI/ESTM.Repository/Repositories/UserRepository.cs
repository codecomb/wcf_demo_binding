﻿/************************************************************************************************************************
* 命名空间: ESTM.Repository.Repositories
* 项目描述: 
* 版本名称: v1.0.0.0
* 作　　者: 唐晓军（QQ:417281862）
* 所在区域: 北京
* 机器名称: DESKTOP-F6QRRBM
* 注册组织: 学科网（www.zxxk.com）
* 项目名称: 学易作业系统
* CLR版本:  4.0.30319.42000
* 创建时间: 2017/7/18 9:45:43
* 更新时间: 2017/7/18 9:45:43
* 
* 功 能： N/A
* 类 名： UserRepository
*
* Ver 变更日期 负责人 变更内容
* ───────────────────────────────────────────────────────────
* V0.01 2017/7/18 9:45:43 唐晓军 初版
*
* Copyright (c) 2017 Lir Corporation. All rights reserved.
*┌──────────────────────────────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．                                                  │
*│　版权所有：北京凤凰学易科技有限公司　　　　　　　　　　　　　                                                      │
*└──────────────────────────────────────────────────────────┘
************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.ComponentModel.Composition;
using ESTM.Domain;
using ESTM.Domain.Model;

namespace ESTM.Repository
{
    //public class UserRepository : IUserRepository,IDependency
    public class UserRepository : EFBaseRepository<User>, IUserRepository,IDependency
    {
      
    }
}
