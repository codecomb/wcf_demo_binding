﻿using ESTM.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESTM.Repository
{
    /// <summary>
    /// 工作单元的实现
    /// </summary>
    public interface IEFUnitOfWork: IUnitOfWorkRepositoryContext
    {
        DbContext context { get; }
    }
}
