﻿using Autofac;
using Autofac.Integration.Mvc;
using ESTM.Domain;
using ESTM.Domain.Model;
using ESTM.Repository;
using Microsoft.Owin;
using Owin;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

[assembly: OwinStartupAttribute(typeof(WebUI.Startup))]
namespace WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<HomeWorkContext, Configuration>());

            var builder = new ContainerBuilder();

            //builder.RegisterType<UnitOfWorkContextBase>().PropertiesAutowired();


            Register(builder);
            ConfigureAuth(app);
        }

        /// <summary>
        /// Autofac提供一个RegisterAssemblyTypes方法。它会去扫描所有的dll并把每个类注册为它所实现的接口。既然能够自动注入，
        /// 那么接口和类的定义一定要有一定的规律。我们可以定义IDependency接口的类型，其他任何的接口都需要继承这个接口。
        /// </summary>
        /// <param name="builder"></param>
        public void Register(ContainerBuilder builder)
        {
            Assembly[] assemblies = Directory.GetFiles(AppDomain.CurrentDomain.RelativeSearchPath, "*.dll").Select(Assembly.LoadFrom).ToArray();
            //注册所有实现了 IDependency 接口的类型
            Type baseType = typeof(IDependency);
            builder.RegisterAssemblyTypes(assemblies)
                   .Where(type => baseType.IsAssignableFrom(type) && !type.IsAbstract)
                   .AsSelf().AsImplementedInterfaces()
                   .PropertiesAutowired().InstancePerLifetimeScope();

            //注册MVC类型
            builder.RegisterControllers(assemblies).PropertiesAutowired();
            builder.RegisterFilterProvider();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }

    public sealed class Configuration : DbMigrationsConfiguration<HomeWorkContext>
    {
        public Configuration()
        {
            //启用自动迁移
            AutomaticMigrationsEnabled = true;
            //获取或设置一个值表示如果在自动数据丢失是可以接受的 [慎重设置]
            //AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(HomeWorkContext context)
        {

        }
    }

}
