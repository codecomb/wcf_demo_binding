﻿using ESTM.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        IUserMangerService _user;

        public HomeController(IUserMangerService _user)
        {
            this._user = _user;
        }


        public ActionResult Index()
        {
            string aaa = this._user.GetData(2);

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}