﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESTM.WCF.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            var oBootstrapper = new Bootstrapper();
            oBootstrapper.StartServices();
            Console.ReadKey();
        }
    }
}
