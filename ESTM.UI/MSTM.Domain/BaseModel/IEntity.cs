﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESTM.Domain
{
    /// <summary>
    /// 用作泛型约束，表示继承自该接口的为领域实体
    /// </summary>
    public interface IEntity
    {
    }

    public class Entity
    {
    }
}
