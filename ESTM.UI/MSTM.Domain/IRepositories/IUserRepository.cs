﻿using ESTM.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESTM.Domain
{
    /// <summary>
    /// 用户仓储接口
    /// </summary>
    public interface IUserRepository:IRepository<User>, IDependency
    {       
    }
}
