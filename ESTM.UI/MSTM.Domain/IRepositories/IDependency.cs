﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESTM.Domain
{
    /// <summary>
    /// 依赖注入接口，表示该接口的实现类将自动注册到容器中
    /// </summary>
    public interface IDependency
    {
    }
}
