﻿/************************************************************************************************************************
* 命名空间: ESTM.Domain.Model
* 项目描述: 
* 版本名称: v1.0.0.0
* 作　　者: 唐晓军（QQ:417281862）
* 所在区域: 北京
* 机器名称: DESKTOP-F6QRRBM
* 注册组织: 学科网（www.zxxk.com）
* 项目名称: 学易作业系统
* CLR版本:  4.0.30319.42000
* 创建时间: 2017/7/18 14:57:20
* 更新时间: 2017/7/18 14:57:20
* 
* 功 能： N/A
* 类 名： ESTMContainer
*
* Ver 变更日期 负责人 变更内容
* ───────────────────────────────────────────────────────────
* V0.01 2017/7/18 14:57:20 唐晓军 初版
*
* Copyright (c) 2017 Lir Corporation. All rights reserved.
*┌──────────────────────────────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．                                                  │
*│　版权所有：北京凤凰学易科技有限公司　　　　　　　　　　　　　                                                      │
*└──────────────────────────────────────────────────────────┘
************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESTM.Domain
{
    [Export(typeof(DbContext))]
    public partial class ESTMContainer : DbContext
    {
        public ESTMContainer() : base("name=JQZYConnString")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }

        public DbSet<User> TB_DEPARTMENT { get; set; }
        //public DbSet<TB_MENU> TB_MENU { get; set; }
        //public DbSet<TB_MENUROLE> TB_MENUROLE { get; set; }
        //public DbSet<TB_ROLE> TB_ROLE { get; set; }
        //public DbSet<TB_USERROLE> TB_USERROLE { get; set; }
        //public DbSet<TB_USERS> TB_USERS { get; set; }
    }
}
