namespace ESTM.Domain.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class HomeWorkContext : DbContext,IDependency
    {
        public HomeWorkContext()
            : base("name=HomeWorkContext")
        {
        }

        public virtual DbSet<Answer> Answer { get; set; }
        public virtual DbSet<Post> Post { get; set; }
        public virtual DbSet<Question> Question { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Answer>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<Answer>()
                .Property(e => e.Content)
                .IsUnicode(false);

            modelBuilder.Entity<Answer>()
                .Property(e => e.AnswerTo)
                .IsUnicode(false);

            modelBuilder.Entity<Answer>()
                .Property(e => e.UserId)
                .IsUnicode(false);

            modelBuilder.Entity<Post>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<Post>()
                .Property(e => e.Content)
                .IsUnicode(false);

            modelBuilder.Entity<Post>()
                .Property(e => e.CommentTo)
                .IsUnicode(false);

            modelBuilder.Entity<Post>()
                .Property(e => e.UserId)
                .IsUnicode(false);

            modelBuilder.Entity<Question>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<Question>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Question>()
                .Property(e => e.Content)
                .IsUnicode(false);

            modelBuilder.Entity<Question>()
                .Property(e => e.UserId)
                .IsUnicode(false);

            modelBuilder.Entity<Tag>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<Tag>()
                .Property(e => e.TagTo)
                .IsUnicode(false);

            modelBuilder.Entity<Tag>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Id)
                .IsUnicode(false);
        }
    }
}
