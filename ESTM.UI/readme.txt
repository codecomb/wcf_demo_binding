表现层：MVC的Web项目，负责UI呈现。

应用层：WCF服务，负责协调领域层的调用，向UI层提供需要的接口。

领域层：定义领域实体和领域逻辑。

基础设施层：一些通用的技术，比如AOP、MEF注入、通用的工具类、DTO模型层，这里为什么要有一个DTO模型层，DTO是用于UI展现用的纯数据Model，它不包含实体行为，是一种贫血的模型。

整个项目的调用方式严格按照DDD设计来进行，UI层通过WCF服务调用应用层的WCF接口，WCF服务通过仓储调用领域层里面的接口，基础设施层贯穿其他各层，在需要的项目中都可以引用基础设施层里面的内库。


http://www.cnblogs.com/landeanfen/p/4834730.html